window.addEventListener('DOMContentLoaded', async () => {
    const location_url = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(location_url);
        if (response.ok){
            const data = await response.json(location_url);
            console.log(data);

            const selectTag = document.getElementById('location');

            for (let location of data.locations){
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event =>{
                event.preventDefault();

                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));

                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
                const conference_url = 'http://localhost:8000/api/conferences/';
                const formResponse = await fetch(conference_url, fetchConfig);
                if (formResponse.ok){
                    formTag.reset();
                }
            })
        }
    } catch(e){
        console.log(e);
    }
})
