function createCard(title, description, pictureUrl, starts, ends, location) {
  return `
    <div class="col-4 g3">
      <div class="card mb-4 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${ location }</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <p class="card-text">${starts} - ${ends}</p>
        </div>
      </div>
    </div>
  `;
}

function createAlert(error){
  return `
  <div class="alert alert-primary" role="alert">
  ${error}
  </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const html = showError(response.status, response.statusText)

    } else {
      const data = await response.json();

      // const conference = data.conferences[0];
      // const nameTag = document.querySelector('.card-title');
      // nameTag.innerHTML = conference.name;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date(details.conference.starts).toDateString();
          const ends = new Date(details.conference.ends).toDateString();
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, starts, ends, location);
          const row = document.querySelector('.row');
          row.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.log(e);
    window.alert('ERROR RAN OUT OF COFFEE!!!')
  }

});
