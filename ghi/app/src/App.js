import logo from './logo.svg';
import Nav from './Nav';
import './App.css';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConference from './AttendConference';



function App(props) {
    if (props.attendees === undefined) {
        return null;
    }
    return (
        <>
        <Nav />
        <div className="container">
          <AttendConference />
        {/* < ConferenceForm /> */}
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
      </>
    );
  }

  export default App;
