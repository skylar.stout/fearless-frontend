import React, { useEffect, useState } from 'react';

function AttendConference(){
    const [conferences, setConferences] = useState([]);

    const [conference, setConference] = useState('');

    const handleChooseConference = (event) => {
        const value = event.target.value;
        setConference(value);
        console.log(value);
    }

    const [name, setName] = useState('');

    const handleName = (event) => {
        const value = event.target.value;
        setName(value);
        console.log(value);
    }

    const [email, setEmail] = useState('');

    const handleEmail = (event) => {
        const value = event.target.value;
        setEmail(value);
        console.log(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();

          const selectTag = document.getElementById('conference');
          for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
          }
        }
      }

      const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.conference = conference;
        data.email = email;
        console.log(data);


        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const AddAttendeeUrl = "http://localhost:8001/api/attendees/"
        const response = await fetch(AddAttendeeUrl, fetchConfig);

        if (response.ok) {
            setName('');
            setEmail('');
            setConference('');
          }

      }

      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
            <img src="/logo.svg" />
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>It's Conference Time!</h1>
              <h2>Please choose what conference you'd like to attend.</h2>
              <form onSubmit={handleSubmit} id="create-location-form">
              <div className="mb-3">
                <select onChange={handleChooseConference} required name="conference" id="conference" className="form-select" value={conference} >
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
  return (
    <option key={conference.href} value={conference.href}>
      {conference.name}
    </option>
  );
})}
                </select>
                </div>
                <p>Now, tell us about yourself.</p>
                <div className="form-floating mb-3">
                  <input onChange={handleName} placeholder="Your full name" required type="text" name="name" id="name" className="form-control" value={name} />
                  <label htmlFor="name">Full Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmail} placeholder="Your email address" required type="email" name="email" id="email" className="form-control" value={email} />
                  <label htmlFor="email">Email</label>
                </div>
                <button className="btn btn-primary">I'm going!</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default AttendConference;
